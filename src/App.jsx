import Layout from "@/components/layout/layout";
import { Analytics } from "@vercel/analytics/react";
import { SonnerToasterContainer } from "./components/ui/sonner";

function App() {
  return (
    <>
      <Layout />
      <SonnerToasterContainer position="bottom-center" closeButton />
      <Analytics />
    </>
  );
}

export default App;
