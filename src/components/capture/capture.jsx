import { Button } from "@/components/ui/button";
import { formatDate } from "@/lib/utils/shared/formatDate";
import { useBoundStore } from "@/store/store";
import { useToPng } from "@hugocxl/react-to-image";
import { Camera } from "lucide-react";
import React from "react";
import { toast } from "sonner";

import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/components/ui/tooltip";

export const CaptureScreen = React.forwardRef(({ ...props }, ref) => {
  const copyToClipboard = useBoundStore((state) => state.copyToClipboard);
  const activePixelRatio = useBoundStore((state) => state.pixelRatio);

  const [_, convert] = useToPng({
    pixelRatio: activePixelRatio.pixelRatio,
    selector: "#capture-container",
    onSuccess: async (data) => {
      if (copyToClipboard) {
        const blob = await fetch(data).then((res) => res.blob());
        await navigator.clipboard.write([
          new ClipboardItem({
            "image/png": blob,
          }),
        ]);
        toast.message("Screenshot copied to the clipboard!");
      }
      const link = document.createElement("a");
      link.download = `fancy_screen-${formatDate(new Date())}`;
      link.href = data;
      link.click();
    },
  });

  const handleConverting = () => {
    convert();
  };

  return (
    <TooltipProvider>
      <Tooltip>
        <TooltipTrigger asChild>
          <Button
            onClick={handleConverting}
            size="sm"
            variant="ghost"
            className="hover:bg-primary hover:text-accent"
          >
            <Camera className="w-4 h-4" />
          </Button>
        </TooltipTrigger>
        <TooltipContent>Capture!</TooltipContent>
      </Tooltip>
    </TooltipProvider>
  );
});

CaptureScreen.displayName = "CaptureScreen";
