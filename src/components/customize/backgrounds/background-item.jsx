import { RadioGroupItem } from "@/components/ui/radio-group";
import { cn } from "@/lib/utils/cn";
import { useBoundStore } from "@/store/store";
import { motion } from "framer-motion";
import { memo } from "react";

export const ColorItem = memo(({ ...props }) => {
  const { id, name, background } = props;

  const activeBackground = useBoundStore((state) => state.background);

  return (
      <motion.label
        className="box-border relative flex flex-col w-full p-1.5 border rounded-md cursor-pointer hover:scale-105 transition-transform"
      >
        <RadioGroupItem value={props} id={id} className="sr-only" />
        <div
          className={cn(
            "relative rounded-md h-36 overflow-hidden transition-all"
          )}
        >
          <div
            className={cn("absolute inset-0 rounded-sm overflow-hidden")}
            style={{ background: background }}
          />
          {activeBackground.id == id && (
            <motion.div
              className="absolute rounded-full border-[4px] border-white size-5 bottom-2 right-2 shadow-sm rotate-180"
              style={{background: background}}
            />
          )}
        </div>
        <p className="py-2 text-sm font-medium">{name}</p>
      </motion.label>

  );
});

ColorItem.displayName = "ColorItem";
