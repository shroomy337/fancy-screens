import { cn } from '@/lib/utils/cn';
import { useBoundStore } from '@/store/store';
import { AnimatePresence, motion } from 'framer-motion';
import { memo } from 'react';

const BackgroundSurface = () => {

    const activeBackground = useBoundStore((state) => state.background);


  return (
    <div
    className={cn(
      "relative rounded w-full h-56 aspect-video overflow-hidden shadow"
    )}
    style={{ background: activeBackground.background }}
  >
    <AnimatePresence mode="wait">
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      key={activeBackground.background}
      className={cn("absolute inset-0")}
      style={{ background: activeBackground.background }}
    />
  </AnimatePresence>
  </div>
  )
}

const MemoizedBackgroundSurface = memo(BackgroundSurface)

export {
    MemoizedBackgroundSurface as BackgroundSurface
};

