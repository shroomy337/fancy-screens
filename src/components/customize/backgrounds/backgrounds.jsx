import { Badge } from "@/components/ui/badge";
import { ScrollArea } from "@/components/ui/scroll-area";
import { Separator } from "@/components/ui/separator";
import { Tabs, TabsList, TabsTrigger } from "@/components/ui/tabs";
import { colors } from "@/lib/utils/enum/colors";
import { gradients } from "@/lib/utils/enum/gradients";
import { useState, useTransition } from "react";
import { BackgroundSurface } from "./background-surface";
import { GradientTabContent } from "./gradient-tab-content";
import { SolidTabContent } from "./solid-tab-content";



export const Backgrounds = () => {

  const [_, startTransition] = useTransition()
  const [tab, setTab] = useState('solid')

  return (
    <Tabs  value={tab} onValueChange={(newValue) => {
      startTransition(() => {
          setTab(newValue)
      })
    }} className="flex flex-col gap-4">
      <TabsList className="w-fit">
        <TabsTrigger value="solid">
          Solid
          <Badge variant="secondary" className="ml-2">
            {colors.length}
          </Badge>
        </TabsTrigger>
        <TabsTrigger value="gradient">
          Gradient
          <Badge variant="secondary" className="ml-2">
            {gradients.length}
          </Badge>
        </TabsTrigger>
      </TabsList>
      <BackgroundSurface />
      <Separator orientation="horizontal" />
      <ScrollArea>
        <SolidTabContent />
        <GradientTabContent />
      </ScrollArea>
    </Tabs>
  );
};
