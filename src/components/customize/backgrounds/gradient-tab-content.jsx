import { RadioGroup } from '@/components/ui/radio-group'
import { TabsContent } from '@/components/ui/tabs'
import { gradients } from '@/lib/utils/enum/gradients'
import { cellWidth, rowHeight } from '@/lib/utils/enum/virtualized.enum'
import { useBoundStore } from '@/store/store'
import { VirtualizedGrid } from '@mierak/react-virtualized-grid'
import { AnimatePresence } from 'framer-motion'
import { memo } from 'react'
import { ColorItem } from './background-item'

const GradientTabContent = () => {

    const pickBackground = useBoundStore((state) => state.pickBackground);

  return (
    <TabsContent value="gradient">
    <RadioGroup onValueChange={(color) => pickBackground(color)}>
    <AnimatePresence mode="wait">
      <VirtualizedGrid
        key="grid-gradient"
        className="p-2"
        itemCount={gradients.length}
        rowHeight={rowHeight}
        cellWidth={cellWidth}
        gridGap={16}
      >
        {(index) => (
          <ColorItem {...gradients[index]} key={gradients[index].id} />
        )}
      </VirtualizedGrid>
    </AnimatePresence>
    </RadioGroup>
  </TabsContent>
  )
}

const MemoizedGradientTabContent = memo(GradientTabContent)

export {
    MemoizedGradientTabContent as GradientTabContent
}

