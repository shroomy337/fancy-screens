import { ColorItem } from "@/components/customize/backgrounds/background-item";
import { RadioGroup } from "@/components/ui/radio-group";
import { TabsContent } from "@/components/ui/tabs";
import { colors } from "@/lib/utils/enum/colors";
import { cellWidth, rowHeight } from "@/lib/utils/enum/virtualized.enum";
import { useBoundStore } from "@/store/store";
import { VirtualizedGrid } from "@mierak/react-virtualized-grid";
import { AnimatePresence } from "framer-motion";
import { memo } from "react";
const SolidTabContent = () => {

  const pickBackground = useBoundStore((state) => state.pickBackground);

  return (
    <TabsContent value="solid">
    <RadioGroup onValueChange={(color) => pickBackground(color)}>
    <AnimatePresence mode="wait">
    <VirtualizedGrid
          key="solid-grid"
          itemCount={colors.length}
          rowHeight={rowHeight}
          cellWidth={cellWidth}
          gridGap={16}
          className="px-0"
        >
          {(index) => (
            <ColorItem {...colors[index]} key={colors[index].id} />
          )}
        </VirtualizedGrid>
      </AnimatePresence>
    </RadioGroup>
  </TabsContent>
  )
}

const MemoizedSolidTabContent = memo(SolidTabContent)

export {
  MemoizedSolidTabContent as SolidTabContent
};

