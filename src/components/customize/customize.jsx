// import { Backgrounds } from "@/components/customize/backgrounds/backgrounds";
import { Export } from "@/components/customize/export/export";
import { Textures } from "@/components/customize/textures/textures";
import { Button, buttonVariants } from "@/components/ui/button";
import { Dialog, DialogContent } from "@/components/ui/dialog";
import { Tabs, TabsContent, TabsTrigger } from "@/components/ui/tabs";
import { cn } from "@/lib/utils/cn";
import { TabsList } from "@radix-ui/react-tabs";
import {
  Bolt,
  Cctv,
  Donut,
  MessagesSquare,
  MountainSnow,
  SwatchBook,
} from "lucide-react";

import { Donations } from "@/components/customize/donations/donations";
import { Feedback } from "@/components/customize/feedback/feedback";
import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/components/ui/tooltip";
import { useMediaQuery } from "@mantine/hooks";
import { useState } from "react";
import { Drawer, DrawerContent } from "../ui/drawer";
import { Backgrounds } from "./backgrounds/backgrounds";

export const Customize = () => {
  const [open, setOpen] = useState(false);

  const isDesktop = useMediaQuery("(min-width: 1024px)");

  const [snap, setSnap] = useState("148px");

  if (isDesktop) {
    return (
      <TooltipProvider>
        <Dialog open={open} onOpenChange={setOpen}>
          <Tooltip>
            <TooltipTrigger asChild>
              <Button
                onClick={() => setOpen(true)}
                variant="ghost"
                size="sm"
                className="hover:bg-primary hover:text-accent"
              >
                <Bolt className="size-4" />
              </Button>
            </TooltipTrigger>
            <TooltipContent>Settings and preferences</TooltipContent>
          </Tooltip>
          <DialogContent className="max-w-[95dvw] w-[1440px] flex p-0 h-[80dvh] z-[70]">
            <Tabs defaultValue="textures" className="flex w-full">
              <TabsList className="w-[240px] border-r bg-muted flex flex-col gap-4">
                <div className="flex flex-col flex-1">
                  <div className="box-border flex flex-col px-1 my-4">
                    <p className="px-3 mb-2 text-sm text-muted-foreground">
                      Preferences
                    </p>
                    <div className="flex flex-col gap-1">
                      <TabsTrigger
                        value="textures"
                        className={cn(
                          buttonVariants({ variant: "ghost" }),
                          "justify-start w-full h-9 gap-2"
                        )}
                      >
                        <MountainSnow className="size-4 text-muted-foreground" />
                        Textures
                      </TabsTrigger>
                      <TabsTrigger
                        value="background"
                        className={cn(
                          buttonVariants({ variant: "ghost" }),
                          "justify-start w-full h-9 gap-2"
                        )}
                      >
                        <SwatchBook className="size-4 text-muted-foreground" />
                        Background
                      </TabsTrigger>
                    </div>
                  </div>
                  <div className="box-border flex flex-col px-1 my-4">
                    <p className="px-3 mb-2 text-sm text-muted-foreground">
                      Settings
                    </p>
                    <div className="flex flex-col gap-1">
                      <TabsTrigger
                        value="export"
                        className={cn(
                          buttonVariants({ variant: "ghost" }),
                          "justify-start w-full h-9 gap-2"
                        )}
                      >
                        <Cctv className="size-4 text-muted-foreground" />
                        Export
                      </TabsTrigger>
                    </div>
                  </div>
                </div>
                <div className="box-border flex w-full gap-1 p-1 border-t">
                  <TabsTrigger
                    value="donations"
                    className={cn(
                      buttonVariants({ variant: "ghost" }),
                      "justify-center h-9"
                    )}
                  >
                    <Donut className="size-4 text-muted-foreground" />
                  </TabsTrigger>
                  <TabsTrigger
                    value="feedback"
                    className={cn(
                      buttonVariants({ variant: "ghost" }),
                      "justify-start w-full h-9 gap-2 flex"
                    )}
                  >
                    <MessagesSquare className="size-4 text-muted-foreground" />
                    Feedback
                  </TabsTrigger>
                </div>
              </TabsList>
              <div className="w-full p-6 overflow-hidden">
                <TabsContent value="textures">
                  <h2 className="mb-6 text-xl font-semibold">Textures</h2>
                  <Textures />
                </TabsContent>
                <TabsContent value="background">
                  <h2 className="mb-6 text-xl font-semibold">Background</h2>
                  <Backgrounds />
                </TabsContent>
                <TabsContent value="export">
                  <h2 className="mb-6 text-xl font-semibold">
                    Export settings
                  </h2>
                  <Export />
                </TabsContent>
                <TabsContent value="donations">
                  <h2 className="mb-6 text-xl font-semibold">Donations</h2>
                  <Donations />
                </TabsContent>
                <TabsContent value="feedback">
                  <h2 className="mb-6 text-xl font-semibold">Feedback</h2>
                  <Feedback />
                </TabsContent>
              </div>
            </Tabs>
          </DialogContent>
        </Dialog>
      </TooltipProvider>
    );
  } else {
    return (
      <Drawer open={open} onOpenChange={setOpen}>
        <Button
          onClick={() => setOpen(true)}
          variant="ghost"
          size="sm"
          className="hover:bg-primary hover:text-accent"
        >
          <Bolt className="size-4" />
        </Button>
        <DrawerContent className="max-h-[97%] h-full bg-muted z-[70]">
          <div className="overflow-y-auto">
            <Tabs defaultValue="textures" className="flex w-full flex-col">
              <TabsList className="bg-muted flex gap-4 justify-between border-b sticky top-0 z-10 ">
                <div className="flex flex-wrap">
                  <div className="box-border flex flex-col px-1 my-4">
                    <p className="px-3 mb-2 text-sm text-muted-foreground">
                      Preferences
                    </p>
                    <div className="flex flex-col gap-1">
                      <TabsTrigger
                        value="textures"
                        className={cn(
                          buttonVariants({ variant: "ghost" }),
                          "justify-start w-full h-9 gap-2"
                        )}
                      >
                        <MountainSnow className="size-4 text-muted-foreground" />
                        Textures
                      </TabsTrigger>
                      <TabsTrigger
                        value="background"
                        className={cn(
                          buttonVariants({ variant: "ghost" }),
                          "justify-start w-full h-9 gap-2"
                        )}
                      >
                        <SwatchBook className="size-4 text-muted-foreground" />
                        Background
                      </TabsTrigger>
                    </div>
                  </div>
                  <div className="box-border flex flex-col px-1 my-4">
                    <p className="px-3 mb-2 text-sm text-muted-foreground">
                      Settings
                    </p>
                    <div className="flex flex-col gap-1">
                      <TabsTrigger
                        value="export"
                        className={cn(
                          buttonVariants({ variant: "ghost" }),
                          "justify-start w-full h-9 gap-2"
                        )}
                      >
                        <Cctv className="size-4 text-muted-foreground" />
                        Export
                      </TabsTrigger>
                    </div>
                  </div>
                  <div className="box-border flex flex-col px-1 my-4">
                    <p className="px-3 mb-2 text-sm text-muted-foreground">
                      Other
                    </p>
                    <TabsTrigger
                      value="donations"
                      className={cn(
                        buttonVariants({ variant: "ghost" }),
                        "justify-start h-9 gap-2"
                      )}
                    >
                      <Donut className="size-4 text-muted-foreground" />
                      Donut
                    </TabsTrigger>
                    <TabsTrigger
                      value="feedback"
                      className={cn(
                        buttonVariants({ variant: "ghost" }),
                        "justify-start w-full h-9 gap-2 flex"
                      )}
                    >
                      <MessagesSquare className="size-4 text-muted-foreground" />
                      Feedback
                    </TabsTrigger>
                  </div>
                </div>
              </TabsList>
              <div className="w-full p-4 lg:p-6 bg-background">
                <TabsContent value="textures">
                  <h2 className="mb-6 text-xl font-semibold">Textures</h2>
                  <Textures />
                </TabsContent>
                <TabsContent value="background">
                  <h2 className="mb-6 text-xl font-semibold">Background</h2>
                  <Backgrounds />
                </TabsContent>
                <TabsContent value="export">
                  <h2 className="mb-6 text-xl font-semibold">
                    Export settings
                  </h2>
                  <Export />
                </TabsContent>
                <TabsContent value="donations">
                  <h2 className="mb-6 text-xl font-semibold">Donations</h2>
                  <Donations />
                </TabsContent>
                <TabsContent value="feedback">
                  <h2 className="mb-6 text-xl font-semibold">Feedback</h2>
                  <Feedback />
                </TabsContent>
              </div>
            </Tabs>
          </div>
        </DrawerContent>
      </Drawer>
    );
  }
};
