import { Separator } from "@/components/ui/separator";

import { Button } from "@/components/ui/button";
import {
  Card,
  CardContent,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { cn } from "@/lib/utils/cn";
import { useEffect, useState } from "react";

export const Donations = () => {
  return (
    <div className="flex flex-col gap-4">
      <div>
        <p className="text-sm text-muted-foreground leading-7 [&:not(:first-child)]:mt-6">
          Hey there! 👋 We're all about making your life easier. This snapshot
          app is designed to be super simple, so you can capture moments
          effortlessly and share them with friends, family, or colleagues in
          just a few clicks.
        </p>
        <p className="text-sm text-muted-foreground leading-7 [&:not(:first-child)]:mt-6">
          I understand how valuable your time is, and I'm here to help you make
          the most of it. By supporting this app, you're not only helping me
          keep the lights on but also contributing to a community of users who
          value simplicity and efficiency.
        </p>
        <p className="text-sm text-muted-foreground leading-7 [&:not(:first-child)]:mt-6">
          Every donation, big or small, goes a long way in keeping this app
          running smoothly and improving it for you and others. Plus, it puts a
          big smile on our faces knowing that we're making your day a little
          brighter!
        </p>
      </div>
      <Separator />
      <Card>
        <CardHeader>
          <CardTitle>Web3</CardTitle>
          <CardDescription>Support Us with Crypto!</CardDescription>
        </CardHeader>
        <CardContent>
          <div className="flex sm:flex-row flex-col items-baseline justify-start">
            <span>
              <code className="text-sm">0x</code> wallet:
            </span>
            <PopoverTimer
              className="truncate inline-block max-w-full"
              title="0x3329a696e6255991fc21fb1a762406fcf432c7cb"
              content="Successfully copied!"
            />
          </div>
        </CardContent>
      </Card>
    </div>
  );
};

const PopoverTimer = ({ title, cleanTimer = 1300, content, className }) => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    let timer = setTimeout(() => {
      setOpen(false);
    }, cleanTimer);
    return () => {
      clearTimeout(timer);
    };
  }, [open]);

  return (
    <Popover onOpenChange={setOpen} open={open}>
      <PopoverTrigger asChild>
        <Button
          onClick={() => navigator.clipboard.writeText(title)}
          variant="link"
          className={cn("p-0 ml-2", className)}
        >
          {title}
        </Button>
      </PopoverTrigger>
      <PopoverContent
        side="top"
        className="w-auto bg-primary border-0 text-white px-3 py-1.5"
      >
        <small className="text-sm font-medium leading-none">{content}</small>
      </PopoverContent>
    </Popover>
  );
};
