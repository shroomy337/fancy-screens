import { Label } from "@/components/ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Switch } from "@/components/ui/switch";
import {
  aspectRatios,
  pixelRatios,
  resolutions,
} from "@/lib/utils/enum/resolutions";
import { useBoundStore } from "@/store/store";
import { useState } from "react";

export const Export = () => {
  const copyToClipboardValue = useBoundStore((state) => state.copyToClipboard);
  const toggleCopyToClipboard = useBoundStore(
    (state) => state.toggleCopyToClipboard
  );

  return (
    <div className="flex flex-col gap-4">
      <p className="text-sm text-muted-foreground">Set your export settings.</p>
      <div className="grid gap-2">
        <SelectAspectRatio />
        <SelectPixelRatio />
        {/* <SelectResolution /> */}
        <h3 className="my-4 text-lg font-medium">Utilities</h3>
        <div className="flex flex-row items-center justify-between p-4 space-y-2 border rounded-lg">
          <div className="space-y-0.5">
            <Label htmlFor="clipboard">Clipboard</Label>
            <p className="text-[0.8rem] text-muted-foreground">
              Save screenshot to your clipboard.
            </p>
          </div>
          <Switch
            id="clipboard"
            checked={copyToClipboardValue}
            onCheckedChange={toggleCopyToClipboard}
          />
        </div>
      </div>
    </div>
  );
};

const SelectAspectRatio = () => {
  const activeRatio = useBoundStore((state) => state.aspectRatio);

  const [selectValue, setSelectValue] = useState(activeRatio.name);

  const pickRatio = useBoundStore((state) => state.pickRatio);

  return (
    <>
      <Label htmlFor="aspectRatio">Aspect ratio</Label>
      <Select
        value={selectValue}
        onValueChange={(element) => {
          setSelectValue(element);
          const searchedRatio = aspectRatios.find(
            (ratio) => ratio.name == element
          );
          pickRatio(searchedRatio);
        }}
      >
        <SelectTrigger id="aspectRatio" className="w-2/3">
          <SelectValue defaultValue={selectValue} placeholder="Select ratio" />
        </SelectTrigger>
        <SelectContent>
          {aspectRatios.map((element) => (
            <SelectItem key={element.name} value={element.name}>
              {element.name}
            </SelectItem>
          ))}
        </SelectContent>
      </Select>
    </>
  );
};
const SelectPixelRatio = () => {
  const activePixelRatio = useBoundStore((state) => state.pixelRatio);

  const [selectValue, setSelectValue] = useState(activePixelRatio.name);

  const pickRatio = useBoundStore((state) => state.pickPixelRatio);

  return (
    <>
      <Label htmlFor="aspectRatio">Pixel ratio</Label>
      <Select
        value={selectValue}
        onValueChange={(element) => {
          setSelectValue(element);
          const searchedRatio = pixelRatios.find(
            (ratio) => ratio.name == element
          );
          pickRatio(searchedRatio);
        }}
      >
        <SelectTrigger id="aspectRatio" className="w-2/3">
          <SelectValue defaultValue={selectValue} placeholder="Select ratio" />
        </SelectTrigger>
        <SelectContent>
          {pixelRatios.map((element) => (
            <SelectItem key={element.name} value={element.name}>
              {element.name}
            </SelectItem>
          ))}
        </SelectContent>
      </Select>
    </>
  );
};

const SelectResolution = () => {
  const activeResolution = useBoundStore((state) => state.resolution);

  const [selectValue, setSelectValue] = useState(activeResolution.name);

  const pickResolution = useBoundStore((state) => state.pickResolution);

  return (
    <>
      <Label htmlFor="resolution">Resolition</Label>
      <Select
        value={selectValue}
        onValueChange={(element) => {
          setSelectValue(element);
          const searchedResolution = resolutions.find(
            (resolution) => resolution.name == element
          );
          pickResolution(searchedResolution);
        }}
      >
        <SelectTrigger id="resolution" className="w-2/3">
          <SelectValue defaultValue={selectValue} placeholder="Select ratio" />
        </SelectTrigger>
        <SelectContent>
          {resolutions.map((element) => (
            <SelectItem key={element.name} value={element.name}>
              {element.name}
            </SelectItem>
          ))}
        </SelectContent>
      </Select>
    </>
  );
};
