import { Separator } from "@/components/ui/separator";

import { Button } from "@/components/ui/button";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { useEffect, useState } from "react";

export const Feedback = () => {
  return (
    <div className="flex flex-col gap-4">
      <p className="text-sm text-muted-foreground leading-7 [&:not(:first-child)]:mt-6">
        Thank you for using my app! Your feedback helps me improve. If you have
        any suggestions, feature requests, or encounter any issues, please feel
        free to reach out to me. I'd love to hear from you!
      </p>

      <Separator />
      <Card>
        <CardHeader>
          <CardTitle>Email</CardTitle>
        </CardHeader>
        <CardContent>
          <div className="flex justify-start text-sm">
            <PopoverTimer
              title="fancysnapss@gmail.com"
              content="Successfully copied!"
            />
          </div>
        </CardContent>
      </Card>
      {/* <Card>
        <CardHeader>
          <CardTitle>Socials</CardTitle>
        </CardHeader>
        <CardContent>
          <div className="flex items-center justify-start">
            <span className="flex items-center">
              <Telegram className="w-5 h-5 mr-2" /> Telegram:
            </span>
            <PopoverTimer title="@yapanmykyta" content="Successfully copied!" />
          </div>
        </CardContent>
      </Card> */}
    </div>
  );
};

const PopoverTimer = ({ title, cleanTimer = 1300, content }) => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    let timer = setTimeout(() => {
      setOpen(false);
    }, cleanTimer);
    return () => {
      clearTimeout(timer);
    };
  }, [open]);

  return (
    <Popover onOpenChange={setOpen} open={open}>
      <PopoverTrigger asChild>
        <Button
          onClick={() => navigator.clipboard.writeText(title)}
          variant="link"
          className="p-0 ml-2"
        >
          {title}
        </Button>
      </PopoverTrigger>
      <PopoverContent
        side="top"
        className="w-auto bg-primary border-0 text-white px-3 py-1.5"
      >
        <small className="text-sm font-medium leading-none">{content}</small>
      </PopoverContent>
    </Popover>
  );
};
