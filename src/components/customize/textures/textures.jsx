import { TextureBlock } from "@/components/texture/texture";
import { Badge } from "@/components/ui/badge";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Separator } from "@/components/ui/separator";
import { cn } from "@/lib/utils/cn";
import { newTextures } from "@/lib/utils/enum/new/textures";
import { textures } from "@/lib/utils/enum/textures";
import { useBoundStore } from "@/store/store";
import { AnimatePresence, motion } from "framer-motion";
import { Fragment } from "react";

export const Textures = () => {
  const pickedTexture = useBoundStore((state) => state.texture);
  const pickTexture = useBoundStore((state) => state.pickTexture);

  return (
    <Fragment>
      <AnimatePresence mode="wait">
        <motion.div
          key={pickedTexture.id}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          className={cn(
            "relative border rounded w-full h-56 aspect-video mb-4 overflow-hidden shadow-sm ",
            pickedTexture.shouldInvert && "bg-black"
          )}
        >
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            className={cn(
              "absolute inset-0",
              pickedTexture.background,
              pickedTexture.background == "bg-wavy" &&
                "opacity-[0.1!important]",
              pickedTexture.background == "bg-circles" &&
                "opacity-[0.7!important]",
              pickedTexture?.className
            )}
          />
        </motion.div>
      </AnimatePresence>
      <Separator orientation="horizontal" className="mb-4" />
      <RadioGroup
        className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4"
        onValueChange={(texture) => pickTexture(texture)}
      >
        {[...textures]
          .sort((textureA, textureB) => {
            // sorting to have A-z first
            const nameA = textureA.name.toLowerCase();
            const nameB = textureB.name.toLowerCase();
            if (nameA < nameB) return -1;
            if (nameA > nameB) return 1;
            return 0;
          })
          .sort((textureA, textureB) => {
            // sorting to have shouldInvert first
            if (textureA.shouldInvert && !textureB.shouldInvert) return -1;
            if (!textureA.shouldInvert && textureB.shouldInvert) return 1;
            return 0;
          })
          .map((element) => (
            <label key={element.id} className="relative flex flex-col gap-2">
              <RadioGroupItem
                value={element}
                id={element.id}
                className="sr-only"
              />
              <div
                className={cn(
                  "relative border rounded h-36 overflow-hidden transition-all shadow-sm",
                  element.shouldInvert && "bg-black"
                )}
              >
                <TextureBlock pickedTexture={element} />
                {newTextures.includes(element.id) && (
                  <Badge className="absolute top-2 right-1">New</Badge>
                )}
                <AnimatePresence mode="wait">
                  {pickedTexture.id == element.id && (
                    <motion.div
                      key={element.id}
                      initial={{ opacity: 0, scale: 0 }}
                      animate={{ opacity: 1, scale: 1 }}
                      exit={{ opacity: 0, scale: 0 }}
                      transition={{
                        ease: "easeInOut",
                        type: "spring",
                        duration: 0.7,
                      }}
                      className="absolute rounded-full border-[4px] w-5 h-5 bottom-2 right-2 border-white bg-primary shadow"
                    />
                  )}
                </AnimatePresence>
              </div>
              <div className="text-sm">
                <h3 className="font-medium leading-none">{element.name}</h3>
              </div>
            </label>
          ))}
      </RadioGroup>
    </Fragment>
  );
};
