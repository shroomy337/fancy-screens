import { Sheets } from "@/components/sheets/sheets";
import { Texture } from "@/components/texture/texture";
import { Toolbar } from "@/components/toolbar/toolbar";
import { cn } from "@/lib/utils/cn";
import { toBase64 } from "@/lib/utils/shared/toBase64";
import { useBoundStore } from "@/store/store";
import { AnimatePresence, motion } from "framer-motion";
import { useCallback, useMemo, useState, useTransition } from "react";
import { toast } from "sonner";

const Layout = () => {
  const activeBackground = useBoundStore((state) => state.background);
  const activeAspectRatio = useBoundStore((state) => state.aspectRatio);

  const [_, startTransition] = useTransition()

  const [activeImage, setActiveImage] = useState("");

  const memoizedActiveImage = useMemo( () => activeImage, [activeImage])

  const memoizedSetActiveImage = useCallback((image) => {
    setActiveImage(image);
  }, []);

  const checkClipboard = async (event) => {
    try {
      const clipboardItems = await event.clipboardData.items;

      if (!clipboardItems || clipboardItems.length === 0) {
        throw new Error(
          "Clipboard is empty or doesn't contain accessible data."
        );
      }

      if (clipboardItems[0].kind == "string") {
        throw new Error("You can paste only media objects.");
      }

      startTransition(async () => {

        const blob = await clipboardItems[0].getAsFile();
        const based = await toBase64(blob);
        setActiveImage(based);
      })

    } catch (error) {
      toast.error("Operation failed", {
        description: error.message.toString(),
      });
    }
  };

  return (
    <div
      vaul-drawer-wrapper=""
      onPaste={checkClipboard}
      className="flex justify-center overflow-hidden"
      style={{ background: activeBackground.background }} // added this style for better smoothing with motion variants
    >
      <Toolbar />
      <AnimatePresence mode="wait">
        <motion.main
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          id="capture-container"
          className={cn(
            "flex items-center justify-center max-w-full h-[100dvh] relative",
            activeAspectRatio.ratio
          )}
          style={{
            background: activeBackground.background,
          }}
        >
          <Texture />
          <Sheets activeImage={memoizedActiveImage} setActiveImage={memoizedSetActiveImage} />
        </motion.main>
      </AnimatePresence>
    </div>
  );
};

export default Layout;
