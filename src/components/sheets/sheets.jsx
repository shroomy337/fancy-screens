import { UploadImage } from "@/components/sheets/upload/upload-image";
import { AnimatePresence, motion } from "framer-motion";

import {
  ContextMenu,
  ContextMenuContent,
  ContextMenuItem,
  ContextMenuTrigger,
} from "@/components/ui/context-menu";
import { Trash } from "lucide-react";
import { memo } from "react";

 const Sheets = ({ activeImage, setActiveImage }) => {
  const noImage = Object.keys(activeImage).length === 0;


  return (
    <AnimatePresence mode="wait">
      {noImage ? (
        <UploadImage key="upload-image" setActiveImage={setActiveImage} />
      ) : (
        <ContextMenu>
          <ContextMenuTrigger asChild>
            <motion.img
              src={activeImage}
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              className="z-50 flex p-2 -m-2 shadow-xl max-w-[75%] max-h-[75dvh] rounded-xl ring-1 ring-inset ring-gray-900/10 bg-gray-900/10 lg:-m-4 lg:p-4 group backdrop-blur supports-[backdrop-filter]:bg-gray-900/10"
            />
          </ContextMenuTrigger>
          <ContextMenuContent className="w-56">
            <ContextMenuItem
              className="text-destructive focus:text-destructive"
              onClick={() => setActiveImage("")}
            >
              <Trash className="w-4 h-4 mr-2" />
              Delete
            </ContextMenuItem>
          </ContextMenuContent>
        </ContextMenu>
      )}
    </AnimatePresence>
  );
};

const MemoizedSheets = memo(Sheets)

export {
  MemoizedSheets as Sheets
};
