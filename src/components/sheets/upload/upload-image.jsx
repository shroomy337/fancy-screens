import { toBase64 } from "@/lib/utils/shared/toBase64";
import { CloudUpload, Loader2 } from "lucide-react";
import { useRef, useState, useTransition } from "react";

import { Button } from "@/components/ui/button";
import { cn } from "@/lib/utils/cn";
import { motion } from "framer-motion";
import { toast } from "sonner";

const MAX_FILE_SIZE = 50000000; // 50MB
const ALLOWED_FILE_TYPES = ["image/png", "image/jpeg", "image/jpg"];

const validateFileType = (file) => {
  return ALLOWED_FILE_TYPES.includes(file.type);
};

export const UploadImage = ({ setActiveImage }) => {
  const ref = useRef();

  const [isImageLoading, setIsImageLoading] = useState(false);
  const [dragActive, setDragActive] = useState(false);

  const [_, startTransition] = useTransition()

  const handleChange = async (e) => {
    e.preventDefault();
    try {
      if (e.target.files && e.target.files[0]) {
        // at least one file has been selected
        // validate file type
        const valid = validateFileType(e.target.files[0]);
        if (!valid) {
          return;
        }
        if (e.target.files[0].size > MAX_FILE_SIZE) {
          return;
        } else {
          setIsImageLoading(true);

          startTransition(async () => {

            try {
              const basedImage = await toBase64(e.target.files[0]);
              addFilesToState(basedImage);
            } catch (error) {
              console.log(error);
              throw new Error(error);
            } finally {
              setIsImageLoading(false);
            }
          })

        }
      }
    } catch (error) {
      console.log(error);
      return error;
    }
  };

  const addFilesToState = (images) => {
    startTransition(() => {
      setActiveImage(images);
    })
  };

  // triggers when file is dropped
  const handleDrop = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    // validate file type
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      const files = Array.from(e.dataTransfer.files);
      const validFiles = files.filter((file) => validateFileType(file));

      if (files.length !== validFiles.length) {
        toast.error("Invalid file type", {
          description: "Only image files are allowed.",
        });
      }
      startTransition(async () => {

        try {
          const basedImage = await toBase64(e.dataTransfer.files[0]);
          addFilesToState(basedImage);
          e.dataTransfer.clearData();
          setDragActive(false);
        } catch (error) {
          console.log(error);
          setActiveImage("");
          return error;
        }

      })
    }
  };

  const handleDrag = (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (e.type === "dragenter" || e.type === "dragover") {
      setDragActive(true);
    } else if (e.type === "dragleave") {
      setDragActive(false);
    }
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <div className="absolute inset-0 flex items-center justify-center">
        <div
          onDragEnter={handleDrag}
          onDragLeave={handleDrag}
          onDragOver={handleDrag}
          onDrop={handleDrop}
          className={cn(
            "w-[75dvw] aspect-video rounded border-2 border-dashed transition backdrop-blur z-40 supports-[backdrop-filter]:bg-border/10",
            !dragActive && "opacity-0"
          )}
        />
      </div>
      <input
        ref={ref}
        onChange={handleChange}
        type="file"
        accept="image/png, image/jpeg, image/jpg"
        className="hidden"
      />
      {!dragActive && (
        <Button
          type="button"
          disabled={isImageLoading}
          className={cn("z-50 transition relative", dragActive && "opacity-0")}
          onClick={() => ref.current.click()}
        >
          {isImageLoading ? (
            <>
              <Loader2 className="w-4 h-4 mr-2 animate-spin" />
              Processing...
            </>
          ) : (
            <>
              <CloudUpload className="w-4 h-4 mr-2" />
              Upload image
            </>
          )}
        </Button>
      )}
    </motion.div>
  );
};
