import { cn } from "@/lib/utils/cn";
import { useBoundStore } from "@/store/store";
import { AnimatePresence, motion } from "framer-motion";

export const TextureBlock = ({ pickedTexture }) => {
  switch (pickedTexture.id) {
    default:
      return (
        <motion.div
          key={pickedTexture.id}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          className={cn(
            "absolute inset-0 z-0",
            pickedTexture.background,
            pickedTexture.background == "bg-grainy" &&
              "opacity-[0.3!important]",
            pickedTexture.background == "bg-wavy" && "opacity-[0.1!important]",
            pickedTexture.background == "bg-circles" &&
              "opacity-[0.7!important]",
            pickedTexture?.className
          )}
        />
      );
  }
};

export const Texture = () => {
  const pickedTexture = useBoundStore((state) => state.texture);

  return (
    <AnimatePresence>
      <TextureBlock pickedTexture={pickedTexture} />
    </AnimatePresence>
  );
};
