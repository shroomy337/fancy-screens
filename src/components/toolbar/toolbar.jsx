import { CaptureScreen } from "@/components/capture/capture";
import { Customize } from "@/components/customize/customize";
import { Button } from "@/components/ui/button";
import { Separator } from "@/components/ui/separator";
import { resetAllSlices } from "@/store/store";
import { Eraser } from "lucide-react";
import React from "react";

import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from "@/components/ui/tooltip";

export const Toolbar = React.forwardRef(({ ...props }, ref) => {
  return (
    <div className="fixed z-50 flex items-center justify-center w-full">
      <div className="flex p-2 mt-4 rounded-lg shadow-lg ring-1 ring-inset ring-gray-900/10 bg-gray-900/10 backdrop-blur supports-[backdrop-filter]:bg-gray-900/10">
        <div className="flex gap-1 p-1 rounded-md bg-muted">
          <TooltipProvider>
            <Tooltip>
              <TooltipTrigger asChild>
                <Button
                  variant="ghost"
                  size="sm"
                  onClick={() => resetAllSlices()}
                  className="hover:bg-primary hover:text-accent"
                >
                  <Eraser className="w-4 h-4" />
                </Button>
              </TooltipTrigger>
              <TooltipContent align="start">
                <p>Reset preferences</p>
              </TooltipContent>
            </Tooltip>
          </TooltipProvider>
          <Customize />
          <div className="block">
            <Separator orientation="vertical" />
          </div>
          <CaptureScreen />
        </div>
      </div>
    </div>
  );
});

Toolbar.displayName = "Toolbar";
