import { CircleCheck, Info, TriangleAlert, XCircle } from "lucide-react";
import { Toaster as Sonner } from "sonner";

const SonnerToasterContainer = ({ ...props }) => {
  return (
    <Sonner
      theme="system"
      className="toaster group pointer-events-auto"
      toastOptions={{
        classNames: {
          toast:
            "group toast border group-[.toaster]:bg-background group-[.toaster]:text-foreground group-[.toaster]:border-border group-[.toaster]:shadow-lg group-[.toaster]:pointer-events-auto",
          description: "group-[.toast]:text-muted-foreground",
          actionButton:
            "group-[.toast]:bg-primary group-[.toast]:text-primary-foreground",
          cancelButton:
            "group-[.toast]:bg-muted group-[.toast]:text-muted-foreground",
          closeButton:
            "absolute right-1 top-4 hover:bg-muted hover:text-foreground rounded-md p-1 transition-opacity text-muted-foreground focus:outline-none focus:ring-1 group-[.destructive]:text-red-300 group-[.destructive]:hover:text-red-50 group-[.destructive]:focus:ring-red-400 group-[.destructive]:focus:ring-offset-red-600 left-[unset] ring-primary",
        },
      }}
      icons={{
        success: <CircleCheck className="size-6 fill-emerald-500 text-white" />,
        info: <Info className="size-6 fill-muted-foreground text-white" />,
        warning: <TriangleAlert className="size-6 fill-amber-500 text-white" />,
        error: <XCircle className="size-6 fill-destructive text-white" />,
      }}
      {...props}
    />
  );
};

export { SonnerToasterContainer };
