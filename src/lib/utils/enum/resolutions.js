export const resolutions = [
  { name: "My device", width: undefined, height: undefined },
  { name: "1024x768", width: 1024, height: 768 },
  { name: "1280x800", width: 1280, height: 800 },
  { name: "1366x768", width: 1366, height: 768 },
  { name: "1440x900", width: 1440, height: 900 },
  { name: "1600x900", width: 1600, height: 900 },
  { name: "1680x1050", width: 1680, height: 1050 },
  { name: "1920x1080", width: 1920, height: 1080 },
  { name: "1920x1200", width: 1920, height: 1200 },
  { name: "2560x1440", width: 2560, height: 1440 },
  { name: "3840x2160", width: 3840, height: 2160 },
  { name: "4096x2160", width: 4096, height: 2160 },
  { name: "5120x2880", width: 5120, height: 2880 },
  { name: "7680x4320", width: 7680, height: 4320 },
];

export const aspectRatios = [
  { name: "16:9", ratio: "aspect-video" },
  { name: "1:1", ratio: "aspect-square" },
  { name: "2:3", ratio: "aspect-2/3" },
  { name: "4:3", ratio: "aspect-4/3" },
  { name: "4:5", ratio: "aspect-4/5" },
  { name: "5:7", ratio: "aspect-5/7" },
];

export const pixelRatios = [
  { name: "My device", pixelRatio: undefined },
  { name: "1 (the fastest)", pixelRatio: 1 },
  { name: "2", pixelRatio: 2 },
  { name: "3", pixelRatio: 3 },
  { name: "4 (the slowest, largest)", pixelRatio: 4 },
];
