export const textures = [
  { id: "empty-texture", name: "Empty", background: "bg-none" },
  { id: "grainy-texture", name: "Grainy", background: "bg-grainy" },
  {
    id: "topographic-texture",
    name: "Topography",
    background: "bg-topographic",
  },
  {
    id: "dots-texture",
    name: "Dots",
    background: "bg-dots",
    shouldInvert: true,
  },
  {
    id: "grid-texture",
    name: "Grid",
    background: "bg-grid",
    shouldInvert: true,
  },
  { id: "grid-small-texture", name: "Small grid", background: "bg-grid-small" },
  { id: "wavy-texture", name: "Wavy", background: "bg-wavy" },
  {
    id: "circles-texture",
    name: "Circles",
    background: "bg-circles",
    className: "bg-blend-multiply",
  },
];
