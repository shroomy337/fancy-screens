export const formatDate = (date) => {
  // Extracting date components
  //   let day = String(date.getDate()).padStart(2, "0");
  //   let month = String(date.getMonth() + 1).padStart(2, "0"); // Adding 1 because January is 0
  //   let year = date.getFullYear();

  // Extracting time components
  let hours = String(date.getHours()).padStart(2, "0");
  let minutes = String(date.getMinutes()).padStart(2, "0");
  let seconds = String(date.getSeconds()).padStart(2, "0");

  // Combining components to form the desired format
  return `${hours}:${minutes}:${seconds}`;
};
