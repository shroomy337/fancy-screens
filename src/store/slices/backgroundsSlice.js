import { colors } from "@/lib/utils/enum/colors";
import { sliceResetFns } from "@/store/store";

export const initialBackgroundSlice = {
  background: colors[0], // white background
};

export const createBackgroundSlice = (set) => {
  sliceResetFns.add(() => set(initialBackgroundSlice));

  return {
    ...initialBackgroundSlice,
    pickBackground: (newBackground) =>
      set(() => ({ background: newBackground })),
  };
};
