import {
  aspectRatios,
  pixelRatios,
  resolutions,
} from "@/lib/utils/enum/resolutions";
import { sliceResetFns } from "@/store/store";

export const initialResolutionSlice = {
  pixelRatio: pixelRatios[0], // device
  aspectRatio: aspectRatios[0], // 16:9 aspectRatio,
  resolution: resolutions[12], // 1920x1080
  copyToClipboard: false,
};

export const createResolutionSlice = (set) => {
  sliceResetFns.add(() => set(initialResolutionSlice));
  return {
    ...initialResolutionSlice,
    pickRatio: (newResolution) => set(() => ({ aspectRatio: newResolution })),
    pickPixelRatio: (newPixelRatio) =>
      set(() => ({ pixelRatio: newPixelRatio })),
    pickResolution: (newResolution) =>
      set(() => ({ resolution: newResolution })),
    toggleCopyToClipboard: (boolean) =>
      set(() => ({ copyToClipboard: boolean })),
  };
};
