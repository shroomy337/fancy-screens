import { textures } from "@/lib/utils/enum/textures";
import { sliceResetFns } from "@/store/store";

export const initialTextureSlice = {
  texture: textures[1], // grainy texture
};

export const createTextureSlice = (set) => {
  sliceResetFns.add(() => set(initialTextureSlice));
  return {
    ...initialTextureSlice,
    pickTexture: (newTexture) => set(() => ({ texture: newTexture })),
  };
};
