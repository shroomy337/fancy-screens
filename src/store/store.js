import { create } from "zustand";
import { createJSONStorage, persist } from "zustand/middleware";

import { createBackgroundSlice } from "./slices/backgroundsSlice";
import { createResolutionSlice } from "./slices/resolutionSlice";
import { createTextureSlice } from "./slices/texturesSlice";

export const sliceResetFns = new Set();

export const resetAllSlices = () => {
  sliceResetFns.forEach((resetFn) => {
    resetFn();
  });
};

export const useBoundStore = create(
  persist(
    (set, get) => ({
      ...createTextureSlice(set, get),
      ...createBackgroundSlice(set, get),
      ...createResolutionSlice(set, get),
    }),
    { name: "options-store", storage: createJSONStorage(() => localStorage) },
  ),
);
